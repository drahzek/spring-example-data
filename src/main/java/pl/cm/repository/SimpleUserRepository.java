
package pl.cm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;

import pl.cm.model.User;

@Transactional
public interface SimpleUserRepository extends CrudRepository<User, Long> , QueryByExampleExecutor<User> {

	Optional<User> findByUsername(String username);

	User findByTheUsersName(String username);

	List<User> findByLastname(String lastname);

	Long removeByLastname(String lastname);

	Slice<User> findByLastnameOrderByUsernameAsc(String lastname, Pageable page);

	List<User> findFirst2ByOrderByLastnameAsc();

	@Query("select u from User u where u.firstname = :name or u.lastname = :name")
	List<User> findByFirstnameOrLastname(@Param("name") String name);

	@Query("select u from User u where u.firstname = :#{#user.firstname} or u.lastname = :#{#user.lastname}")
	Iterable<User> findByFirstnameOrLastname(@Param("user") User user);

	List<User> findByAgeGreaterThan(Integer age);

	List<User> findByAgeLessThan(Integer age);

	List<User> findByAgeBetween(Integer age1, Integer age2);

	List<User> findByAgeBetweenOrderByAgeAsc(Integer age1, Integer age2);

	List<User> findFirst5ByAgeBetweenOrderByAgeAsc(Integer age1, Integer age2);

    Slice<User> findByAgeBetweenOrderByAgeAsc(Integer age1, Integer age2, Pageable page);

    List<User> findByFirstnameContainsOrderByFirstnameDesc(@Param("name") String name);

    @Query("select u from User u where u.firstname like ?1%")
    List<User> findByFirstnameStartsWithOrderByLastname(@Param("name") String name);

	@Query("select u from User u where u.age > :age and u.lastname like %:name")
	List<User> findByFirstnameEndsWithAndAgeGreaterThan(@Param("age") Integer age, @Param("name") String name);


//	List<User> findMatching(String name, String surname);

}

package pl.cm;

import static java.lang.System.out;

import java.util.List;
import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;

import pl.cm.model.User;
import pl.cm.model.UserBuilder;
import pl.cm.repository.SimpleUserRepository;

public class App {

  public static void main(String[] args){
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-configuration.xml");
    final SimpleUserRepository userRepository = context.getBean(SimpleUserRepository.class);

    out.println("Find by username optional result");
    final Optional<User> optionalByUsername = userRepository.findByUsername("itzel.cassin");
    out.println(optionalByUsername);

    out.println("Find by username ");
    final User byUsername = userRepository.findByTheUsersName("itzel.cassin");
    out.println(byUsername);

    out.println("Find by lastName");
    final List<User> byLastname = userRepository.findByLastname("White");
    out.println(byLastname);

    out.println("Remove");
    final Long removedCount = userRepository.removeByLastname("MacGyver");
    out.println(removedCount);

    out.println("Pageable");
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(0,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(1,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(2,2)));

    out.println("Find first 2 with order");
    final List<User> first2ByOrderByLastnameAsc = userRepository.findFirst2ByOrderByLastnameAsc();
    out.println(first2ByOrderByLastnameAsc);

    out.println("Find first 2 with order");
    final List<User> byFirstnameOrLastname = userRepository.findByFirstnameOrLastname("White");
    out.println(byFirstnameOrLastname);

    out.println("Find first 2 with order SpEl");
    final User user = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byFirstnameOrLastnameSpEl = userRepository.findByFirstnameOrLastname(user);
    out.println(byFirstnameOrLastnameSpEl);


    out.println("Find By example firsname");
    final User userByExample = UserBuilder.anUser().withFirstname("White").build();
    final Iterable<User> byExampleFirstName = userRepository.findAll(Example.of(userByExample));
    out.println(byExampleFirstName);

    out.println("Find By example firsname and lastname");
    final User userByExampleAnd = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleAnd = userRepository.findAll(Example.of(userByExampleAnd));
    out.println(byExampleAnd);

    out.println("Find By example firsname or lastname");
    final User userByExampleOr = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleOr = userRepository.findAll(Example.of(userByExampleOr, ExampleMatcher.matchingAny()));
    out.println(byExampleOr);

    out.println("Find By age greater than");
    final List<User> byAgeGreaterThan = userRepository.findByAgeGreaterThan(90);
    out.println(byAgeGreaterThan);

    out.println("Find By age lesser than");
    final List<User> byAgeLessThan = userRepository.findByAgeLessThan(20);
    out.println(byAgeLessThan);

    out.println("Find By age between");
    final List<User> byAgeBetween = userRepository.findByAgeBetween(10, 20);
    out.println(byAgeBetween);

    out.println("Find By age between order by age asc");
    final List<User> byAgeBetweenOrderByAgeAsc = userRepository.findByAgeBetweenOrderByAgeAsc(10, 20);
    out.println(byAgeBetweenOrderByAgeAsc);

    out.println("Find first 5 By age between order by age asc");
    final List<User> first5ByAgeBetweenOrderByAgeAsc = userRepository.findFirst5ByAgeBetweenOrderByAgeAsc(10, 20);
    out.println(first5ByAgeBetweenOrderByAgeAsc);

    out.println("Pageable exercise");
    out.println(userRepository.findByAgeBetweenOrderByAgeAsc(80,90, new PageRequest(0,5)));
    out.println(userRepository.findByAgeBetweenOrderByAgeAsc(80,90, new PageRequest(1,5)));

    out.println("Find by firstname containing order by firstname desc");
    final List<User> byFirstnameContainsOrderByFirstnameDesc = userRepository.findByFirstnameContainsOrderByFirstnameDesc("Jai");
    out.println(byFirstnameContainsOrderByFirstnameDesc);

    out.println("9.Find by firstname containing ordered by lastname");
    final List<User> byFirstnameStartsWithOrderByLastname = userRepository.findByFirstnameStartsWithOrderByLastname("Jai");
    out.println(byFirstnameStartsWithOrderByLastname);

    out.println("10.Find by firstname starting with and age greater than");
    final List<User> byFirstnameEndsWithAndAgeGreaterThan = userRepository.findByFirstnameEndsWithAndAgeGreaterThan(24,"ton");
    out.println(byFirstnameEndsWithAndAgeGreaterThan);

//    out.println("Zadanie 11");
//    final User user1 = UserBuilder.anUser().withFirstname("connor").build();
//    final Iterable<User> byExampleName = userRepository.findAll(Example.of(user1, ExampleMatcher.matchingAny().withIgnoreCase()));
//    out.println(byExampleName);

    out.println("Zadanie 12");
    final User userByExample1 = UserBuilder.anUser().withFirstname("jesse").withLastname("King").build();
    ExampleMatcher matcher = ExampleMatcher.matching()
            .withMatcher("firstname",ExampleMatcher.GenericPropertyMatchers.ignoreCase())
            .withMatcher("lastname",ExampleMatcher.GenericPropertyMatchers.contains());
    final Iterable<User> byExampleFirstName1 = userRepository.findAll(Example.of(userByExample1,matcher));
    out.println(byExampleFirstName1);
  }
}
